export default function (server) { 
    server.auth.strategy( 'admin', 'bearer-access-token', { 
        validate: async (request, token, h) => { 
            if(token === 'adminToken'){
                return { 
                    isValid: true, 
                    credentials: {}, 
                    artifacts: {}, 
                };
            }
            return { 
                isValid: false, 
                credentials: {}, 
                artifacts: {}, 
            };
        }, 
    }); 
}