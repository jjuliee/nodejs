import {users} from "./controllers.js";

export default function (server) { 
    server.auth.strategy( 'user', 'bearer-access-token', { 
        validate: async (request, token, h) => { 
            const userFind = users.find((item)=>item.token===token);
            if(userFind){
                return { 
                    isValid: true, 
                    credentials: {userFind}, 
                    //credentials: {user: userFind.login}, 
                    artifacts: {}, 
                };
            }
            return { 
                isValid: false, 
                credentials: {}, 
                artifacts: {}, 
            };
        }, 
    }); 
}