import { v4 as uuidv4 } from 'uuid';
const {createHash} = await import('crypto');
import Boom from '@hapi/boom';

export const users = [];
export const comments = [];
export const posts = [];
export let countView = [];


export const controllers =
{
    hello:(request, h) => {
        return 'Hello World!';
    },
    registration:(request, h) => {
        const {password,...rest} = request.payload;
        const passwHash = createHash("md5").update(password).digest("hex");
        const response = {...rest, password: passwHash,id:uuidv4(),token:uuidv4()};
        users.push(response);
        return response.token;
    },
    comment:(request, h) => {
        const {text} = request.payload;
        const userId = request.auth.credentials.userFind.id;
        //console.log(userId);
        const response = {text, id:uuidv4(),userId};
        comments.push(response);
        return comments;
    },
    post:(request, h) => {
        const {text, title} = request.payload;
        const userId = request.auth.credentials.userFind.id;
        const response = {text, title, id:uuidv4(), userId};
        posts.push(response);
        return posts;
    },
    viewpost:(request, h) => { 
        const {postId} = request.payload; 
        const postFind = posts.find((item)=>item.id===postId);  
        if(postFind) {   
            if(!countView[postId]){
                countView[postId] = 1;
            }
            else{
                countView[postId]=countView[postId]+1;
            }
            console.log('Количество просмотров:',countView);
            const count = countView[postId];
            return {postFind,count};
        }
        else{
            console.log('Пост не найден');
            return {};
        }
    }
}