import http from 'http';
import axios from 'axios'; 
import path from 'path'; 
import { URL } from 'url'; 
import fs from 'fs'; 
const __dirname = new URL('.', import.meta.url).pathname;

/*import * as constants from "./constants/index.js";
console.log(constants.color, constants.metadata, constants.user);*/

/*const first = () => {
        console.log("first")
}
const two = (callback) => {
    setTimeout(()=>{
        console.log("two");
        callback();
    },2000);
}
const three = () => {
    console.log("three")
}

first();
two(three);
//three();*/

/*const first = () => {
    const promise = new Promise((resolve, reject) => {
      setTimeout(() => {
        reject(new Error('Error'));
      }, 1000);
    });
    return promise;
  };
  
  const two = () => {
    const promise = new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve('two');
      }, 2000);
    });
    return promise;
  };
  
  const three = () => {
    const promise = new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve('three');
      }, 1000);
    });
    return promise;
  };*/
  
  /*first()
    .then(console.log)
    .then(() => two())
    .then(console.log)
    .then(() => three())
    .then(console.log);*/

    //first().catch((err)=>console.log(err.message));

///////////////////////////////////////////////


    //1)Написать функцию getNumberAsync, которая через 5 секунд возвращает случайное число от 1 до 10.
    /*const getNumberAsync = () => {
      const promise = new Promise((resolve, reject) => {
        setTimeout(() => {
          resolve(Math.floor(Math.random() * 10) + 1);
        }, 5000);
      });
      return promise;
    };*/
    //getNumberAsync().then(console.log);

    //2)Написать функцию sumAsync, которая через 5 секунд возвращает сумму всех переданных в нее параметров.
    /*const sumAsync = (...args) => {
      const promise = new Promise((resolve, reject) => {
        let result=0;
        setTimeout(() => {         
          for (let i = 0; i < args.length; i++) {
            result += args[i];
          }
          resolve(result);
        }, 5000);
      });
      return promise;
    };*/
    //sumAsync(1,1,5).then(console.log);

    /*3. Написать функцию calcThreeNumbersAsync, которая три раза вызывает getNumberAsync, выводит
  каждое из полученных чисел в консоль, после чего передает все три в sumAsync и выводит возвращенную
  из функции сумму в консоль. Убедиться, что функция calcThreeNumbersAsync выполняется не больше 10 секунд.*/
  /*const calcThreeNumbersAsync = async() => {        
        let a=await getNumberAsync();
        let b=await getNumberAsync();
        let c=await getNumberAsync();
        const [ma,mb,mc] = await Promise.all([a,b,c]);
        console.log(ma,mb,mc);
        let d = await sumAsync(ma,mb,mc);
        console.log(d);

  };
    calcThreeNumbersAsync();*/

    /*4. Написать функцию calcNNumbersAsync, которая делает то же что calcThreeNumbersAsync,
  но для N чисел (передается в параметре функции). Убедиться, что функция calcNNumbersAsync
  выполняется не больше 10 секунд.*/
  /*const calcNNumbersAsync = async(n) => { 
    let arr=[]; 
    for(let i=0;i<n;i++){
      arr[i]=await getNumberAsync();
    };     
    let resarr = await Promise.all(arr);
    console.log(resarr);
    let d = await sumAsync(...resarr);
    console.log(d);

};
calcNNumbersAsync(5);*/

/*5. Написать функцию getNumberMoreThanAsync, которая которая через 5 секунд возвращает число от 1 до 10,
  но большее чем то, что было ей передано в параметре. Если ей передать число 10, то должна
  вернуться ошибка (Promise rejected).*/
  /*const getNumberMoreThanAsync = (v) => {
    const promise = new Promise((resolve, reject) => {
      setTimeout(() => {              
        if(v===10) {reject(new Error('Promise rejected'));}
        let m=-3;
        while(m<v){
          m = Math.floor(Math.random() * 10) + 1;
        }
        resolve(m);
      }, 5000);
    });
    return promise;
  };*/
 //getNumberMoreThanAsync(5);

  /*6. Написать функцию calcThreeAscNumbersAsync, которая три раза вызывает getNumberMoreThanAsync,
  получая три числа (каждое последующее больше предыдущего), выводит каждое из полученных чисел в консоль,
  после чего передает все три в sumAsync и выводит возвращенную из функции сумму в консоль.
  Также требуется отловить возможную ошибку и вывести в консоль соответствующее сообщение.*/
  /*const calcThreeAscNumbersAsync = async(v) => {        
    let a=await getNumberMoreThanAsync(v);
    let b=await getNumberMoreThanAsync(a);
    let c=await getNumberMoreThanAsync(b);
    const [ma,mb,mc] = await Promise.all([a,b,c]);
    console.log(ma,mb,mc);
    let d = await sumAsync(ma,mb,mc);
    console.log(d);
};
calcThreeAscNumbersAsync(1);*/

/*const server = http.createServer((req,res)=>{res.end("hello")});
server.listen(3000,'127.0.0.1');*/


const server = http.createServer((req,res)=>{
  console.log('URL страницы', req.url);
  res.writeHead(200, {'Content-Type':'text/plain; charset=utf-8'});
  const myWriteShort = fs.createReadStream(__dirname + 'dump.txt','utf8');
  myWriteShort.pipe(res);
});
server.listen(3000,'127.0.0.1');
console.log('Сервер запущен на 127.0.0.1:3000');
