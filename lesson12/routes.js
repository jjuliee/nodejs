import Joi from 'joi';
import {controllers} from './controllers.js';

export default[
{
    method: 'GET',
    path: '/{countPets}',
    handler: controllers.hello,
    options:{
        //auth:{strategy:'user'},
        auth:{strategy:'admin'},
        validate:{
            query:Joi.object().keys({
                catName:Joi.string().required().min(3).max(20),
                dogName:Joi.string().optional()
            }),
            params:Joi.object().keys({
                countPets:Joi.number().optional(),
            })
        },
       description:'Получить Hello',
       tags:['api','pets']
    }
},
{
method:'POST',
path: '/user/registration',
handler:controllers.registration,
options:{
    validate:{
        payload:Joi.object().keys({
            login: Joi.string().required(),
            password: Joi.string().required(),
            firstName:Joi.string().required(),
            age:Joi.number().required()
        })
    },
    description:'Регистрация пользователя',
    tags:['api','user']
}
},
{
    method:'POST',
    path: '/user/comment',
    handler:controllers.comment, 
    options:{
        auth:{strategy:'user'},
        validate:{
            payload:Joi.object().keys({
                text: Joi.string().required()
            })
        },
        description:'Комментарии пользователя',
        tags:['api','user']
    }
},
{
    method:'POST',
    path: '/user/post',
    handler:controllers.post, 
    options:{
        auth:{strategy:'user'},
        validate:{
            payload:Joi.object().keys({
                text: Joi.string().required(),
                title: Joi.string().required()
            })
        },
        description:'Создание поста',
        tags:['api','user']
    }
},{
    method:'GET',
    path: '/user/post/{postId}',
    handler:controllers.postGet, 
    options:{
        //auth:{strategy:'user'},
        validate:{
            params:Joi.object().keys({
                postId: Joi.string().required()
            })
        },
        description:'Просмотр поста',
        tags:['api','user']
    }
}]