import mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

export const userSchema = new mongoose.Schema({
    login:{
        type:String,
        required:true
    },
    password:{
        type:String,
        required:true
    },
    age:{
        type:Number,
        required:false
    },
    firstName:{
        type:String,
        required:true
    },
    token:{
        type:String,
        required:true,
        default:uuidv4
    },
})