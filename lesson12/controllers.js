import { v4 as uuidv4 } from 'uuid';
const {createHash} = await import('crypto');
import Boom from '@hapi/boom';

export const users = [];
export const comments = [];
export const posts = [];


export const controllers =
{
    hello:(request, h) => {
        return 'Hello World!';
    },
    registration:(request, h) => {
        const {password,...rest} = request.payload;
        const passwHash = createHash("md5").update(password).digest("hex");
        const response = {...rest, password: passwHash,id:uuidv4(),token:uuidv4()};
        users.push(response);
        return response.token;
    },
    comment:(request, h) => {
        const {text} = request.payload;
        const userId = request.auth.credentials.userFind.id;
        //console.log(userId);
        const response = {text, id:uuidv4(),userId};
        comments.push(response);
        return comments;
    },
    post:(request, h) => {
        const {text, title} = request.payload;
        const userId = request.auth.credentials.userFind.id;
        const response = {text, title, id:uuidv4(), userId,countView:0};
        posts.push(response);
        return posts;
    },
    postGet:(request, h) => { 
        const {postId} = request.params; 
        const postFind = posts.find((item)=>item.id===postId);  
        if(postFind) {   
            postFind.countView+=1;
            return {postFind};
        }
        else{
            return Boom.notFound(`Ошибка, пост с id ${postId} не найден`);
        }
    }
}