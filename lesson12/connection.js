import mongoose from 'mongoose';
import { userSchema } from './userSchema.js';

const dbName = 'myFirstDataBase';
const uri = `mongodb://localhost:27017/${dbName}`;
mongoose.connect(uri, { useNewUrlParser: true, 
    useUnifiedTopology: true });
const db = mongoose.connection;
db.on('error', (err) =>
  console.log('Произошла ошибка при подключении к базе данных!', err)
);
db.once('open', () => console.log('Успешно подключились к Монге!'));

const User = mongoose.model('User', userSchema);

export default { User };
