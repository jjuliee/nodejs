'use strict';

import Hapi from '@hapi/hapi';
//import Boom from '@hapi/boom';
import Inert from '@hapi/inert';
import Vision from '@hapi/vision';
import HapiSwagger from'hapi-swagger';
import AuthBearer from 'hapi-auth-bearer-token';
import routes from './routes.js';
import userAuth from './userAuth.js';
import adminAuth from './adminAuth.js';
import database from './connection.js';


const init = async () => {

    const server = Hapi.server({
        port: 3000,
        host: 'localhost'
    });

    const swaggerOptions = {
        info: {
                title: 'Test API Documentation',
                version: '1.0.0',
            },
        };

    await server.register([
        Inert,
        Vision,
        AuthBearer,
        {
            plugin: HapiSwagger,
            options: {...swaggerOptions,securityDefinitions:{
                Bearer:{
                    type: 'apiKey',
                    name: 'Authorization',
                    description: 'Bearer token',
                    in: 'header',
                },
            },
            security:[{Bearer:[]}],
            },
        },
    ]);

    userAuth(server);
    adminAuth(server);
    server.route(routes);
    await server.start();
    console.log('Documentation in', `${server.info.uri}/documentation`);
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

init();